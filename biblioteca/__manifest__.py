# -*- coding: utf-8 -*-
{
    'name': "Biblioteca",

    'summary': """
        Gestiona la biblioteca y los pedidos que se hacen""",

    'description': """
        Gestiona los librros, prestamos, los socios y los pedidos que se hacen en una biblioteca sin ningun esfuerzo.
    """,

    'author': "Javier Juan Uceda",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',
    'application': True,

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'seguridad/ir.model.access.csv',
        'vistas/libro.xml',
        'vistas/autor.xml',
        'vistas/editorial.xml',
        'vistas/lineapedido.xml',
        'vistas/localizacion.xml',
        'vistas/pedido.xml',
        'vistas/prestamo.xml',
        'vistas/retirada.xml',
        'vistas/socio.xml',
	'vistas/usuario.xml',
        'vistas/tipo.xml'
    ],
}
