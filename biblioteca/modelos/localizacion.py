# -*- coding: utf-8 -*-
from odoo import models, fields


class localizacion(models.Model):
    _name = 'biblioteca.localizacion'
    nombre = fields.Char(string='Nombre', required=True)

    # Weak entity
    _sql_constraints = [
        ('weak_entity_localizacion_unique',
         'UNIQUE (id, nombre)',
         '')]
