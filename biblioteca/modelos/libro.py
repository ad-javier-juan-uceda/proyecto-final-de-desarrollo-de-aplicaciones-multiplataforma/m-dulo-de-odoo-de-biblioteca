# -*- coding: utf-8 -*-
from odoo import models, fields


class libro(models.Model):
    _name = 'biblioteca.libro'

    ESTADOS = [
        ('0', 'Buen estado'),
        ('1', 'Estropeado'),
        ('2', 'Inutilizable'),
        ('3', 'Paginas rotas'),
    ]

    titulo = fields.Char(string='Titulo del libro', required=True)
    isbn = fields.Char(string='ISBN del libro', required=True)
    estado = fields.Selection(ESTADOS, default=ESTADOS[0][0], string="Estado del libro", required=True)
    cantidad = fields.Integer(string='Cantidad', required=True)
    paginas = fields.Integer(string='Paginas que tiene el libro', required=True)
    prestado = fields.Boolean(string='Esta prestado', required=True)
    autor_id = fields.Many2one('biblioteca.autor', 'Autor', required=True)
    editorial_id = fields.Many2one('biblioteca.editorial', 'Editorial', required=True)
    localizacion_id = fields.Many2one('biblioteca.localizacion', 'Localizacion', required=True)
    tipo_id = fields.Many2one('biblioteca.tipo', 'Tipo del libro', required=True)

    # Weak entity
    _sql_constraints = [
        ('weak_entity_libro_unique',
         'UNIQUE (titulo, id, isbn)',
         'No puede haber mas de un libro con el mismo titulo ni con el mismo ISBN. Tienes que cambiarlo.')]

