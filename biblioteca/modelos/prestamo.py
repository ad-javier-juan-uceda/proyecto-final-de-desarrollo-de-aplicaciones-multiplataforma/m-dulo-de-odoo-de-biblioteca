# -*- coding: utf-8 -*-
from odoo import models, fields


class prestamo(models.Model):
    _name = 'biblioteca.prestamo'

    libro_id = fields.Many2one('biblioteca.libro', 'Libro prestado', required=True,ondelete='cascade')
    socio_id = fields.Many2one('biblioteca.socio', 'Socio al que se lo presta', required=True)
    fecha_de_prestamo = fields.Date(string='Fecha que se presta el libro')
    fecha_de_devolucion = fields.Date(string='Fecha que se tiene que devolver')
    fecha_de_devuelto = fields.Date(string='Fecha que se ha devuelto')

    # Weak entity
    _sql_constraints = [
        ('weak_entity_prestamo_unique',
         'UNIQUE (id)',
         '')]
