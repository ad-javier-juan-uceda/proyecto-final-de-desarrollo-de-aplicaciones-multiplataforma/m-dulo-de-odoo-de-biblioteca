# -*- coding: utf-8 -*-
from odoo import models, fields


class retirada(models.Model):
    _name = 'biblioteca.retirada'

    ESTADOS_DE_DEVOLUCION = [
        ('0', 'Buen estado'),
        ('1', 'Estropeado'),
        ('2', 'Inutilizable'),
        ('3', 'Paginas rotas'),
    ]

    fecha_de_inicio_retirada = fields.Date(string='Fecha que se retira el carnet', required=True)
    fecha_de_finalizacion_retirada = fields.Date(string='Fecha quese devuelve el carnet')
    motivo = fields.Selection(ESTADOS_DE_DEVOLUCION, default=ESTADOS_DE_DEVOLUCION[0][0], string="Estado del libro")
    socio_id = fields.Many2one('biblioteca.socio', 'Socio al retira el carnet', required=True)

    # Weak entity
    _sql_constraints = [
        ('weak_entity_retirada_unique',
         'UNIQUE (id)',
         '')]
