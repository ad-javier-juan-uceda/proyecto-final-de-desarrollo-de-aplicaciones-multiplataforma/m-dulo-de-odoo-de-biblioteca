# -*- coding: utf-8 -*-
from odoo import models, fields


class lineapedido(models.Model):
    _name = 'biblioteca.lineapedido'

    pedido_id = fields.Many2one('biblioteca.pedido', 'Pedido', required=True)
    libro_id = fields.Many2one('biblioteca.libro', 'Libro que vas a comprar', required=True, ondelete='cascade')
    cantidad = fields.Integer(string='Cantidad', required=True)
    precio = fields.Float(string='Precio', required=True)

    # Weak entity
    _sql_constraints = [
        ('weak_entity_lineapedido_unique',
         'UNIQUE (id, pedido_id)',
         'Pon otro pedido')]
