# -*- coding: utf-8 -*-
from odoo import models, fields


class usuario(models.Model):
    _name = 'biblioteca.usuario'

    nombre = fields.Char(string='Nombre del socio', required=True)
    dni = fields.Char(string='DNI', size=9, required=True)
    fecha_de_nacimiento = fields.Date(string='Fecha de nacimiento')
    direccion = fields.Char(string='Direccion', required=True)
    localidad = fields.Char(string='Localidad', required=True)
    contrasenya = fields.Char(string='Contraseña', required=True)
    # Weak entity
    _sql_constraints = [
        ('weak_entity_socio_unique',
         'UNIQUE (id,nombre,dni)',
         '')]
