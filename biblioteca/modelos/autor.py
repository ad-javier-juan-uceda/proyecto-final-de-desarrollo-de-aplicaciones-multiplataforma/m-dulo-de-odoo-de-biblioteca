# -*- coding: utf-8 -*-
from odoo import models, fields


class autor(models.Model):
    _name = 'biblioteca.autor'

    nombre = fields.Char(string='Nombre', required=True)
    telefono = fields.Integer(string='Telefono', required=True)
    fecha_de_nacimiento = fields.Date(string='Fecha de nacimiento')
    pagina_web = fields.Char(string='Pagina web')
    facebook = fields.Char(string='Facebook')
    twitter = fields.Char(string='Twitter')
    instagram = fields.Char(string='Instagram')

    # Weak entity
    _sql_constraints = [
        ('weak_entity_autor_unique',
         'UNIQUE (id)',
         '')]
