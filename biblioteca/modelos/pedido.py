# -*- coding: utf-8 -*-
from odoo import models, fields


class pedido(models.Model):
    _name = 'biblioteca.pedido'

    fecha_de_realizacion = fields.Date(string='Fecha que se realiza el pedido', required=True)
    fecha_de_entrega = fields.Date(string='Fecha que entrega el pedido')
    usuario_id = fields.Many2one('biblioteca.usuario', 'Usuario que hace el pedido', required=True)

    # Weak entity
    _sql_constraints = [
        ('weak_entity_pedido_unique',
         'UNIQUE (id)',
         '')]
