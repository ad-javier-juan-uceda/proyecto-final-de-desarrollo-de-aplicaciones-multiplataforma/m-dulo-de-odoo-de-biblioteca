# -*- coding: utf-8 -*-
from odoo import models, fields


class tipo(models.Model):
    _name = 'biblioteca.tipo'
    nombre = fields.Char(string='Nombre', required=True)
    descripcion = fields.Char(string='Descripcion', required=True)

    # Weak entity
    _sql_constraints = [
        ('weak_entity_tipo_unique',
         'UNIQUE (id)',
         '')]
