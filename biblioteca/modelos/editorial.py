# -*- coding: utf-8 -*-
from odoo import models, fields


class editorial(models.Model):
    _name = 'biblioteca.editorial'

    nombre = fields.Char(string='Nombre de la editorial', required=True)
    pagina_web = fields.Char(string='Pagina web')
    direccion = fields.Char(string='Direccion', required=True)
    localidad = fields.Char(string='Localidad', required=True)
    telefono = fields.Integer(string='Telefono', required=True)
    cif = fields.Char(string='CIF', required=True)
    facebook = fields.Char(string='Facebook')
    twitter = fields.Char(string='Twitter')
    instagram = fields.Char(string='Instagram')

    # Weak entity
    _sql_constraints = [
        ('weak_entity_editorial_unique',
         'UNIQUE (CIF, id)',
         'No puede haber mas de una editorial con el mismo CIF. Tienes que cambiarlo.')]
